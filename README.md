A challenge project to create a RESTful API



Comments

Few tests missing


Technical Specification:

1) Maven must be used to build, run tests and start the application.

2) The tests must be started with the mvn test command.

3) The application must start with a Maven command: mvn exec:java, mvn jetty:run, mvn spring-boot:run, etc.

4) The application must have a stateless API and use a database to store data.

5) An embedded in-memory database should be used: either H2, HSQL, SQLite or Derby.

6) The database and tables creation should be done by Maven or by the application.

Install and run the project

1. download/clone the project.

2. Run the following maven command on the console in the root directory of the project.

mvn tomcat7:run

Testing the project

Run the following maven command on the console in the root directory of the project.

mvn test

Endpoints

The next json was used in the follows examples:

{"name":"Barbecue", "images":[{"uri":"https://static.carrefourdigital.com.br/medias/sys_master/images/images/h66/he6/h00/h00/8944087203870.jpg"},{"uri":"https://static.carrefourdigital.com.br/medias/sys_master/images/images/h16/hc8/h00/h00/8944089759774.jpg"}]}

Create a product

Method:POST http://localhost:8080/challenge/product/

Update a product

Method:PUT http://localhost:8080/challenge/product/

Delete a product

Method:DELETE http://localhost:8080/challenge/product/

Get all products with images and child products

Method:GET http://localhost:8080/challenge/product/

Get all products without images and child products

You have to set the header fot images and child products (both are boolean values), so if you want images/child products set true.

Method:GET http://localhost:8080/challenge/product/

Get a product with images and child products

Method:GET http://localhost:8080/challenge/product/{product-id}

Get a product without images and child products

You have to set the header for images and child products (both are boolean values), so if you want images/child products set true.

Method:GET http://localhost:8080/challenge/product/{product-id}

Get the child's products

Method:GET http://localhost:8080/challenge/product/child/{product-id}

Get the image's products

Method:GET http://localhost:8080/challenge/product/image/{product-id}