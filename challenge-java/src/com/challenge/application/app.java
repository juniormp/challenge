package com.challenge.application;

import com.challenge.entity.Address;
import com.challenge.entity.Customer;
import com.challenge.entity.Digital;
import com.challenge.entity.Membership;
import com.challenge.entity.Order;
import com.challenge.entity.Payment;
import com.challenge.entity.PaymentMethod;
import com.challenge.entity.Physical;
import com.challenge.entity.ProductType;

public class app {

	public static void main(String[] args) {
		
		// Usage

		// Customer
		Customer junior = new Customer("Junior", new Address("01010-110"), "junior@gmail.com");
		
		// Product
		Physical clean_code = new Physical("Clean Code", "A Handbook of Agile Software Craftsmanship", ProductType.BOOK, 200.00d);
		Physical battlefield = new Physical("Battle Field 1", "War game", ProductType.GAME, 200.00d);
		Membership ruby = new Membership("Online Course Ruby", "Programming", ProductType.RECCURING, 89.99d);
		Digital la_la_land = new Digital("La la Land", "Online movie", ProductType.STREAM, 30.00d);
		
		// Order	
		Order order = new Order();
		order.addProduct(clean_code);
		order.addProduct(battlefield);
		order.addProduct(ruby);
		order.addProduct(la_la_land);

		// Payment
		Payment payment = new Payment(order, new PaymentMethod());
		payment.isPay();
		payment.pay();
		payment.isPay();
		
		System.out.println("DISCOUNT: " + order.getTotalDiscount());
		System.out.println("PAYMENT WITH DISCOUNT: " + order.getTotalAmount());
		System.out.println("PAYMENT WITHOUT DISCOUNT: " + payment.getAmount());
		
	}

}
