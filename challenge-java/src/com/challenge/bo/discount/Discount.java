package com.challenge.bo.discount;

public interface Discount {

	public double discount();
	
}
