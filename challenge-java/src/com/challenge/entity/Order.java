package com.challenge.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

	private Integer id;
	private Customer customer;
	private List<Product> products;
	private Address adress;
	private LocalDate closedAt;

	public Order() {
		this.products = new ArrayList<>();
	}

	public Order(Customer customer, Address adress, LocalDate closedAt) {
		super();
		this.customer = customer;
		this.adress = adress;
		this.closedAt = closedAt;
		this.products = new ArrayList<>();
	}

	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public List<Product> getProducts() {
		return products;
	}
	
	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	public Address getAdress() {
		return adress;
	}
	
	public void setAdress(Address adress) {
		this.adress = adress;
	}
	
	public LocalDate getClosedAt() {
		return closedAt;
	}
	
	public void setClosedAt(LocalDate closedAt) {
		this.closedAt = closedAt;
	}
	
	public Double getTotalAmount(){
		double totalAmount = 0.0d;
		for (Product product : products) {
			totalAmount += product.getAmount();
		}
		return totalAmount;
	}
	
	public void addProduct(Product product){
		this.products.add(product);
	}
	
	public void close(LocalDate date){
		this.closedAt = date;
	}
	
	public Double getTotalDiscount(){
		double totalDiscount = 0.0d;
		for (Product product : products) {
			totalDiscount += product.discount();
		}
		return totalDiscount;
	}
	
	public void prepareShipping(){
		for (Product product : products) {
			product.shipping();
		}
	}	
}
