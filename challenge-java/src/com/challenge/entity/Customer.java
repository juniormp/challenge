package com.challenge.entity;

public class Customer {

	private Integer id;
	private String name;
	private Address adress;
	private String email;
	
	public Customer() {
		super();

	}
	
	public Customer(String name, Address adress, String email) {
		super();
		this.name = name;
		this.adress = adress;
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Address getAdress() {
		return adress;
	}
	
	public void setAdress(Address adress) {
		this.adress = adress;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
