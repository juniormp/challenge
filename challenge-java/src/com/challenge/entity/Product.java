package com.challenge.entity;

import com.challenge.bo.discount.Discount;
import com.challenge.bo.shipping.Shipping;

public abstract class Product implements Shipping, Discount{

	private Integer id;
	private String name;
	private String description;
	private ProductType productType;
	private Double amount;
	
	public Product() {
		super();
	}

	public Product(String name, String description, ProductType productType, double amount) {
		super();
		this.name = name;
		this.description = description;
		this.productType = productType;
		this.amount = amount;
	}
	
	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public ProductType getproductType() {
		return productType;
	}
	
	public void setproductType(ProductType productType) {
		this.productType = productType;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public void setAmount(double amount) {
		this.amount = amount;
	}	

}
