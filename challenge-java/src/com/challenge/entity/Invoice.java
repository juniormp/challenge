package com.challenge.entity;

public class Invoice {

	private Address billing_address;
	private Address shipping_address;
	private Order order;
	
	public Invoice() {

	}	
	
	public Invoice(Address billing_address, Address shipping_address, Order order) {
		super();
		this.billing_address = billing_address;
		this.shipping_address = shipping_address;
		this.order = order;
	}

	public Address getBilling_address() {
		return billing_address;
	}

	public void setBilling_address(Address billing_address) {
		this.billing_address = billing_address;
	}
	
	public Address getShipping_address() {
		return shipping_address;
	}
	
	public void setShipping_address(Address shipping_address) {
		this.shipping_address = shipping_address;
	}
	
	public Order getOrder() {
		return order;
	}
	
	public void setOrder(Order order) {
		this.order = order;
	}
	
	
	
}
