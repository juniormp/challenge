package com.challenge.entity;

import com.challenge.bo.tax.ProductWithTax;
import com.challenge.bo.tax.ProductWithoutTax;
import com.challenge.bo.tax.TaxRuleTypeProduct;

public enum ProductType {

	BOOK(new ProductWithoutTax()), 
	GAME(new ProductWithTax()), 
	RECCURING(new ProductWithTax()), 
	STREAM(new ProductWithTax());

	private TaxRuleTypeProduct taxRule;

	ProductType(TaxRuleTypeProduct taxRule) {
		this.taxRule = taxRule;
	}

	public TaxRuleTypeProduct getTaxRule() {
		return taxRule;
	}
}
