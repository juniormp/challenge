package com.challenge.entity;

public class Digital extends Product {

	public Digital(String name, String description, ProductType type, double amount) {
		super(name, description, type, amount);
	}

	public Digital() {
		super();
	}

	@Override
	public void shipping() {
		System.out.println("Call a method to send email");
		this.getproductType().getTaxRule().informTax();	
	}

	@Override
	public double discount() {
		double discountForDigitalProduct = 10.00d;
		return discountForDigitalProduct;
	}

}
