package com.challenge.entity;

public class Membership extends Product {

	private boolean membershipStatus;

	public Membership(String name, String description, ProductType type, double amount) {
		super(name, description, type, amount);
	}

	public Membership() {
		super();
	}

	@Override
	public void shipping() {
		System.out.println("Call a method to send email");
		activateMembership();
		this.getproductType().getTaxRule().informTax();	
	}

	public void activateMembership() {
		this.membershipStatus = true;
	}

	@Override
	public double discount() {
		double discountForMembershipProduct = 0.0d;
		return discountForMembershipProduct;
	}
}
