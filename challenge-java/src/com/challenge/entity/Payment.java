package com.challenge.entity;

import java.time.LocalDate;

public class Payment {

	private Integer id;
	private LocalDate authorization_number;
	private Double discount;
	private Double amount;
	private Invoice invoice;
	private Order order;
	private PaymentMethod paymentMethod;
	private LocalDate paidAt;

	public Payment() {
		super();

	}

	public Payment(Order order, PaymentMethod paymentMethod) {
		this.order = order;
		this.paymentMethod = paymentMethod;
	}

	public LocalDate getAuthorization_number() {
		return authorization_number;
	}

	public void setAuthorization_number(LocalDate authorization_number) {
		this.authorization_number = authorization_number;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public LocalDate getPaidAt() {
		return paidAt;
	}

	public void setPaidAt(LocalDate paidAt) {
		this.paidAt = paidAt;
	}

	public void pay() {
		this.discount = order.getTotalDiscount();
		this.amount = order.getTotalAmount() + getDiscount();
		this.authorization_number = LocalDate.now();
		this.paidAt = LocalDate.now();
		order.prepareShipping();
		order.close(this.paidAt);
	}
	
	public boolean isPay(){
		if(paidAt != null){
			return true;
		}else{
			return false;
		}	
	}	
}
