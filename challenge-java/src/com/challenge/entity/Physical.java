package com.challenge.entity;

public class Physical extends Product{

	public Physical(String name, String description, ProductType type, double amount) {
		super(name, description, type, amount);
	}

	public Physical() {
		super();
	}

	@Override
	public void shipping() {
		System.out.println("Call a method to create a shipping label ...");
		this.getproductType().getTaxRule().informTax();		
	}

	@Override
	public double discount() {
		double discountForPhysicalProduct = 0.0d;
		return discountForPhysicalProduct;
	}
}
